# Jean Michel Menu

Jean Michel Menu (JMM) is a self-hosted personnal
assistant for plannings meals.
It runs associated with a nextcloud instance in which the calendar and
the cookbook applications are installed.

It based on the French language for processing the ingredients of
the recipes, some adaptations would be needed to use another language
(I do not plan to do it in a near future, but I'm happy to help).

---

### Note

I built this tool for my own usage, so I do not garanty that it is easy to use or install, and that it is bug free.

---


## Features

* JMM randomly selects menus from the cookbook.
* JMM makes the grocery shopping list based on the selected menus.
* JMM adds all menus to a Nextcloud calendar.
* Based on a simple algorithm, JMM tries to vary the menus between days,
so every selected menu is as different as possible than the previous menus
(see [docs/weights.ipynb](docs/weights.ipynb)).
* JMM only selects menus with meat 3 times per week (hardcoded variable,
that you could adjust) (file [scripts/create_menus.py](scripts/create_menus.py), variable `MAX_MEAT_PER_WEEK`).
The list of ingredients considered as meat is in the file [scripts/database.py](scripts/database.py), variable `list_meat`.
* JMM only selects menus with vegetables available at the date for which menu is
selected (based on the list [data/products.json](data/products.json).

```mermaid
flowchart TD
    A[Launch Jean Michel Menu\n/makemenu on telegram]-- User chooses dates --> B[Select some menus by hand?]
    B -- Yes --> C[Provide the id of the menu\nin the cookbook]
    C --> B
    B -- No / All done ----> D[JMM selects menus for all dates]
    D --> F[Re-run selection of specific menus?]
    F -- Yes --> G[JMM selects another menu for\nthe specified date]
    G --> F
    F -- No / All done --> H[JMM prepares the grocery list]
    H --> I[JMM sends the grocery list on the\nTelegram group specified in settings]
    I --> J[JMM adds all menus to the Nextcloud calendar]
    J --> K{Finished}
```


## Install

`poetry install` to install python packages. Then run


You need to create a `.env` file in root of this folder, see the `example.env` file.


## Run


```
poetry run scripts/telegram_bot.py
```
