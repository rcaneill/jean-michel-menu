from dotenv import dotenv_values
from pathlib import Path
import os

script_path = Path(os.path.dirname(os.path.abspath(__file__)))
config = dotenv_values(script_path / "../.env")

# Telegram
token_telegram_bot = config["token_telegram_bot"]
id_groceries_telegram_bot = config["id_groceries_telegram_bot"]
AUTHORIZED = [int(config[i]) for i in config.keys() if "AUTHORIZED" == i[:10]]
# Nextcloud
USR_nextcloud = config["USR_nextcloud"]
PWD_nextcloud = config["PWD_nextcloud"]
URL_nextcloud = config["URL_nextcloud"]
CAL_id_nextcloud = config["CAL_id_nextcloud"]
ingredient_categories_url = config["URL_ing_cat"]
N_PEOPLE = float(config.get("number_of_people", 3))
