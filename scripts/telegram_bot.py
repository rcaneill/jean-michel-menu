from telegram.update import Update
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    Update,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
    Bot,
    InlineQueryResultArticle,
    InputTextMessageContent,
)
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
    CallbackQueryHandler,
    InlineQueryHandler,
)
import telegram
from telegram.ext import messagequeue as mq

from database import rebase, get_db, convert_ingredient, _reformat, get_db_categories
from cal_funcs import get_past_recipes, add_recipes, create_next_dates
from create_menu import propose_meal_week
from groceries import load_groceries, add_grocery, list_str, write_groceries, empty
from _auth import token_telegram_bot, id_groceries_telegram_bot, AUTHORIZED, N_PEOPLE
from uuid import uuid4

from threading import Thread

db = get_db()

from dateutil import parser
import zoneinfo
import time

filter_authorized_users = Filters.user(user_id=AUTHORIZED)


def start(update: Update, context: CallbackContext):
    update.message.reply_text(
        "Bonjour et bienvenue sur le bot de génération des repas !\
	Écrivez /help pour voir les commandes."
    )


def help(update: Update, context: CallbackContext):
    update.message.reply_text(
        """Commandes :
        /makemenu : propose la liste des menus
        """
    )


def menus(update: Update, context: CallbackContext):
    # Get previous meals
    ids, last_meal_date = get_past_recipes()
    ids = [i for i in ids if i is not None]
    # propose meals
    meals = propose_meal_week(14, db, ids, "automne")
    dates = create_next_dates(last_meal_date, len(meals))
    msg = "\n".join([f"{d} : {m[0]}" for m, d in zip(meals, dates)])
    update.message.reply_text(msg)


def unknown(update: Update, context: CallbackContext):
    update.message.reply_text("Sorry '%s' is not a valid command" % update.message.text)


def unknown_text(update: Update, context: CallbackContext):
    update.message.reply_text(
        "Sorry I can't recognize you , you said '%s'" % update.message.text
    )


(
    SELECTING_LAST_DATE,
    SPECIFY_MENUS_ENFORCED,
    USER_SELECT_MENU,
    GENERATE_MENUS,
    GENERATE_GROCERIES,
    GENERATE_CAL,
) = range(6)


def _str_date(date):
    if 10 < date.hour < 14:
        meal_type = "lunch"
    else:
        meal_type = "diner"
    return f"{date.year}-{date.month}-{date.day} {meal_type}"


def makemenu(update: Update, context: CallbackContext) -> int:
    """Starts the menu creation"""

    # Start by rebasing
    def rebase_and_get_db():
        rebase()
        print("rebased")
        global db
        db = get_db()

    global thread_rebase
    thread_rebase = Thread(target=rebase_and_get_db)
    thread_rebase.start()
    # We get the next possible date from the calendar
    try:
        reply_keyboard = [
            [_str_date(i)] for i in create_next_dates(get_past_recipes()[0][0], 2)
        ]
    except IndexError:
        reply_keyboard = []

    update.message.reply_text(
        "Send /cancel to stop talking to me.\n"
        "Bonjour Maître, mon nom est Jean-Michel et je vais vous aider à préparer vos menus.\n\n"
        "Commencez par indiquer la date du 1er repas sous la forme "
        "yyyy-mm-dd lunch(diner) :",
        quote=False,
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder="Date ?"
        ),
    )
    return SELECTING_LAST_DATE


def selecting_last_date(update: Update, context: CallbackContext) -> int:
    initial_date = parser.parse(
        update.message.text.replace("lunch", "12:00").replace("diner", "19:00")
    ).replace(tzinfo=zoneinfo.ZoneInfo(key="Europe/Stockholm"))
    context.bot_data["initial_date"] = initial_date
    # We propose multiple dates
    reply_keyboard = [
        [f"{n+2}:{_str_date(i)}"]
        for n, i in enumerate(create_next_dates(initial_date, 26))
    ][::-1]
    update.message.reply_text(
        f"Vous avez choisi la date {update.message.text} pour démarrer. "
        "Maintenant choisissez le dernier repas à préparer (nombre de repas) :",
        quote=False,
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder="Date ?"
        ),
    )
    return SPECIFY_MENUS_ENFORCED


def _box(b):
    if b:
        return "\u25A3"
    else:
        return "\u25A2"


VALIDATE_SPECIFY = "validate_specify"
VALIDATE_CHOICE_MENU = "validate_choice_menu"


def make_dates_keyboard(dates):
    keyboard = [
        [
            InlineKeyboardButton(
                _box(dates[i]["manual"]) + " " + _str_date(i), callback_data=str(i)
            )
        ]
        for i in dates
    ]
    keyboard.append([InlineKeyboardButton("Valider", callback_data=VALIDATE_SPECIFY)])
    return keyboard


def make_dates_menus_keyboard(dates):
    keyboard = [
        [
            InlineKeyboardButton(
                _str_date(d) + " : " + dates[d]["meal"][0],
                callback_data=str(d),
            )
        ]
        for d in dates
    ]
    keyboard.append(
        [InlineKeyboardButton("Valider", callback_data=VALIDATE_CHOICE_MENU)]
    )
    return keyboard


def button_effect(update: Update, context: CallbackContext) -> None:
    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query
    # CallbackQueries need to be answered, even if no notification to the user is needed
    # Some clients may have trouble otherwise. See https://core.telegram.org/bots/api#callbackquery
    query.answer()
    d = parser.parse(query.data).replace(
        tzinfo=zoneinfo.ZoneInfo(key="Europe/Stockholm")
    )
    if context.bot_data["select_manually"]:
        context.bot_data["dates"][d]["manual"] = not context.bot_data["dates"][d][
            "manual"
        ]
        query.edit_message_text(
            text="Selectionnez les repas à choisir manuellement",
            reply_markup=InlineKeyboardMarkup(
                make_dates_keyboard(context.bot_data["dates"])
            ),
        )
    else:
        if "history" not in context.bot_data["dates"][d]:
            context.bot_data["dates"][d]["history"] = []
        context.bot_data["dates"][d]["history"].append(
            context.bot_data["dates"][d]["meal"][1]
        )
        ids = get_past_recipes() + [
            (i, context.bot_data["dates"][i]["meal"][1])
            for i in context.bot_data["dates"]
            if i != d
        ]
        try:
            context.bot_data["dates"][d]["meal"] = propose_meal_week(
                db, ids, [d], forbidden_meals=context.bot_data["dates"][d]["history"]
            )[0]
        except ValueError:
            context.bot_data["dates"][d]["history"] = []
            context.bot_data["dates"][d]["meal"] = propose_meal_week(
                db, ids, [d], forbidden_meals=context.bot_data["dates"][d]["history"]
            )[0]
        query.edit_message_text(
            text=f"Voilà les menus proposés :\n",
            reply_markup=InlineKeyboardMarkup(
                make_dates_menus_keyboard(context.bot_data["dates"])
            ),
        )


def specify_menus_enforced(update: Update, context: CallbackContext) -> int:
    context.bot_data["select_manually"] = True
    n_dates = int(update.message.text.split(":")[0])
    dates = create_next_dates(context.bot_data["initial_date"], n_dates - 1)
    dates = [
        context.bot_data["initial_date"],
    ] + dates
    context.bot_data["dates"] = {i: {"manual": False} for i in dates}
    reply_markup = InlineKeyboardMarkup(make_dates_keyboard(context.bot_data["dates"]))
    update.message.reply_text(
        "Merci pour votre choix,",
        reply_markup=ReplyKeyboardRemove(),
        quote=False,
    )
    update.message.reply_text(
        "Selectionnez les repas à choisir manuellement",
        quote=False,
        reply_markup=reply_markup,
    )
    return USER_SELECT_MENU


def user_select_menu(update: Update, context: CallbackContext) -> int:
    # Check if we arrive here from call back or from answer
    if update.callback_query:
        update.callback_query.answer()
        try:
            to_select = [
                i
                for i in context.bot_data["dates"]
                if (context.bot_data["dates"][i]["manual"])
                and (context.bot_data["dates"][i].get("meal", None) is None)
            ][0]
            update.callback_query.message.reply_text(
                f"Répondez l'id de la recette du repas du {_str_date(to_select)}",
                quote=False,
            )
            return USER_SELECT_MENU
        except IndexError:
            # Means that all user selected menus have been chosen
            update.callback_query.message.reply_text(
                f"Nous allons choisir les menus automatiquement",
                quote=False,
                reply_markup=ReplyKeyboardMarkup([["Valider"]], one_time_keyboard=True),
            )
            return GENERATE_MENUS
    else:
        # 1st we add the choice of user to the menus
        d = (
            update.message.reply_to_message.text.replace(
                "Répondez l'id de la recette du repas du ", ""
            )
            .replace("lunch", "12:00")
            .replace("diner", "19:00")
        )
        # Try to get the recipe associated with id
        thread_rebase.join()
        try:
            meal = db[update.message.text]
            context.bot_data["dates"][
                parser.parse(d).replace(
                    tzinfo=zoneinfo.ZoneInfo(key="Europe/Stockholm")
                )
            ]["meal"] = (
                meal["name"],
                meal["id"],
            )
            update.message.reply_text(f"Vous avez choisi {meal['name']}")
        except (ValueError, TypeError):
            update.message.reply_text(
                f"L'id {update.message.text} n'existe pas",
            )
        try:
            to_select = [
                i
                for i in context.bot_data["dates"]
                if (context.bot_data["dates"][i]["manual"])
                and (context.bot_data["dates"][i].get("meal", None) is None)
            ][0]
            update.message.reply_text(
                f"Répondez l'id de la recette du repas du {_str_date(to_select)}",
                quote=False,
            )
            return USER_SELECT_MENU
        except IndexError:
            # Means that all user selected menus have been chosen
            update.message.reply_text(
                f"Nous allons choisir les menus automatiquement",
                quote=False,
                reply_markup=ReplyKeyboardMarkup([["Valider"]], one_time_keyboard=True),
            )
            return GENERATE_MENUS


def generate_menus(update: Update, context: CallbackContext) -> int:
    if context.bot_data["select_manually"]:
        dates = [
            i
            for i in context.bot_data["dates"]
            if not context.bot_data["dates"][i]["manual"]
        ]
        ids = get_past_recipes() + [
            (i, context.bot_data["dates"][i]["meal"][1])
            for i in context.bot_data["dates"]
            if context.bot_data["dates"][i]["manual"]
        ]
    else:
        dates = [
            i
            for i in context.bot_data["dates"]
            # if not context.bot_data["dates"][i]["validated"]
        ]
        ids = get_past_recipes() + [
            (i, context.bot_data["dates"][i]["meal"][1])
            for i in context.bot_data["dates"]
            # if context.bot_data["dates"][i]["validated"]
        ]

    context.bot_data["select_manually"] = False

    thread_rebase.join()
    meals = propose_meal_week(db, ids, dates)
    for m, d in zip(meals, dates):
        context.bot_data["dates"][d]["meal"] = m
    # for d in context.bot_data["dates"]:
    #    context.bot_data["dates"][d]["validated"] = True
    s = ""
    for m, d in zip(meals, dates):
        s += f"{_str_date(d)} : {m[0]}\n"
    update.message.reply_text(
        f"Voilà les menus proposés :\n",
        quote=False,
        reply_markup=InlineKeyboardMarkup(
            make_dates_menus_keyboard(context.bot_data["dates"])
        ),
    )


from time import sleep


def send_m(bot, chatid, msg):
    sleep(5)
    bot.send_message(chatid, msg)


def generate_groceries(update: Update, context: CallbackContext) -> int:
    # Check if we arrive here from call back or from answer
    if update.callback_query:
        update.callback_query.answer()
        context.bot.send_message(
            chat_id=update.callback_query.message.chat_id,
            text=f"Nous allons créer la liste de courses",
            reply_markup=ReplyKeyboardMarkup([["Valider"]], one_time_keyboard=True),
        )
        return GENERATE_GROCERIES
    db_cat = get_db_categories()
    l = load_groceries()
    l_in_stock = []
    meals = [context.bot_data["dates"][d]["meal"] for d in context.bot_data["dates"]]
    for i in meals:
        m = db[i[1]]
        N = int(m["recipeYield"])
        if N == 0:
            N = 2
        for ingredient in m["recipeIngredient"]:
            name, q = convert_ingredient(ingredient)
            q /= N / N_PEOPLE
            l, l_in_stock = add_grocery(l, name, q, l_in_stock)
    l = list_str(l)
    l_sorted = {}
    for i in l:
        cat = db_cat.get(i, "Category to add in database")
        if cat in l_sorted:
            l_sorted[cat].append(i)
        else:
            l_sorted[cat] = [i]
    for cat in l_sorted:
        send_m(context.bot, id_groceries_telegram_bot, f"*** CATEGORY ***\n{cat}")
        for i in l_sorted[cat]:
            q = l[i]
            send_m(context.bot, id_groceries_telegram_bot, f"{i} : {q}")
            print(f"{i} : {q}")
    in_stock_str = "\n".join(l_in_stock)
    send_m(
        context.bot,
        id_groceries_telegram_bot,
        f"Veuillez penser à vérifier vos stock pour :\n{in_stock_str}",
    )
    update.message.reply_text(
        f"Nous allons ajouter les menus au calendrier",
        quote=False,
        reply_markup=ReplyKeyboardMarkup([["Valider"]], one_time_keyboard=True),
    )
    return GENERATE_CAL


def generate_cal(update: Update, context: CallbackContext) -> int:
    dtstarts = [d for d in context.bot_data["dates"]]
    ids = [context.bot_data["dates"][d]["meal"][1] for d in dtstarts]
    add_recipes(db, ids, dtstarts)
    update.message.reply_text(
        f"Les menus ont été ajoutés au calendrier. Terminé !",
        quote=False,
    )
    return ConversationHandler.END


def cancel(update: Update, context: CallbackContext) -> int:
    """Cancels and ends the conversation."""
    user = update.message.from_user
    print(f"User {user.first_name} canceled the conversation.")
    update.message.reply_text(
        "Bye! I hope we can talk again some day.", reply_markup=ReplyKeyboardRemove()
    )

    return ConversationHandler.END


def id(update: Update, context: CallbackContext):
    update.message.reply_text(
        f"Conversation id: {update.message.chat}",
    )


class MQBot(telegram.bot.Bot):
    """A subclass of Bot which delegates send method handling to MQ"""

    def __init__(self, *args, is_queued_def=True, mqueue=None, **kwargs):
        super(MQBot, self).__init__(*args, **kwargs)
        # below 2 attributes should be provided for decorator usage
        self._is_messages_queued_default = is_queued_def
        self._msg_queue = mqueue or mq.MessageQueue()

    def __del__(self):
        try:
            self._msg_queue.stop()
        except:
            pass

    @mq.queuedmessage
    def send_message(self, *args, **kwargs):
        """Wrapped method would accept new `queued` and `isgroup`
        OPTIONAL arguments"""
        return super(MQBot, self).send_message(*args, **kwargs)


# Inline query
def inline_query(update, context) -> None:
    """Handle the inline query. This is run when you type: @botusername <query>"""
    query = update.inline_query.query
    if update.effective_user.id not in AUTHORIZED:
        # Request does not come from an authorized user
        print(
            f"Getting inline query from user {update.effective_user.first_name} {update.effective_user.last_name} ({update.effective_user.id} {update.effective_user.username})"
        )
        return
    if query == "":
        return

    results = [
        InlineQueryResultArticle(
            id=str(uuid4()),
            title=db[i]["name"],
            input_message_content=InputTextMessageContent(db[i]["id"]),
        )
        for i in db
        if (_reformat(query.lower()) in _reformat(db[i]["name"].lower()))
        or len(
            [None for j in db[i]["recipeIngredient"] if _reformat(query.lower()) in j]
        )
        > 0
    ]

    update.inline_query.answer(results)


if __name__ == "__main__":
    from telegram.utils.request import Request

    request = Request(con_pool_size=8)
    q = mq.MessageQueue()
    bot = MQBot(token=token_telegram_bot, request=request, mqueue=q)
    updater = Updater(
        bot=bot,
        use_context=True,
    )
    dispatcher = updater.dispatcher

    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler("makemenu", makemenu, filters=filter_authorized_users)
        ],
        states={
            SELECTING_LAST_DATE: [
                MessageHandler(
                    Filters.text & ~Filters.command & filter_authorized_users,
                    selecting_last_date,
                )
            ],
            SPECIFY_MENUS_ENFORCED: [
                MessageHandler(
                    Filters.text & ~Filters.command & filter_authorized_users,
                    specify_menus_enforced,
                ),
            ],
            USER_SELECT_MENU: [
                CallbackQueryHandler(user_select_menu, pattern=f"^{VALIDATE_SPECIFY}$"),
                MessageHandler(
                    Filters.text & ~Filters.command & filter_authorized_users,
                    user_select_menu,
                ),
            ],
            GENERATE_MENUS: [
                MessageHandler(
                    Filters.text & ~Filters.command & filter_authorized_users,
                    generate_menus,
                ),
                CallbackQueryHandler(
                    generate_groceries, pattern=f"^{VALIDATE_CHOICE_MENU}$"
                ),
            ],
            GENERATE_GROCERIES: [
                MessageHandler(
                    Filters.text & ~Filters.command & filter_authorized_users,
                    generate_groceries,
                )
            ],
            GENERATE_CAL: [
                MessageHandler(
                    Filters.text & ~Filters.command & filter_authorized_users,
                    generate_cal,
                )
            ],
        },
        fallbacks=[CommandHandler("cancel", cancel, filters=filter_authorized_users)],
    )

    dispatcher.add_handler(conv_handler)

    dispatcher.add_handler(CallbackQueryHandler(button_effect))
    dispatcher.add_handler(InlineQueryHandler(inline_query))
    print("start bot")
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()
