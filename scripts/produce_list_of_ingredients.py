#!/usr/bin/env python

from database import rebase, get_db, get_only_ingredient


def main():
    rebase()
    db = get_db()
    all_ingredients = set()
    for i in db:
        all_ingredients.update(get_only_ingredient(db[i]["recipeIngredient"]))
    with open("../.database/ingredients.csv", "w") as f:
        f.writelines("\n".join(all_ingredients))
    print(all_ingredients)
    print("*** DONE ***")


if __name__ == "__main__":
    main()
