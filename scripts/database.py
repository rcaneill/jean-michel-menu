import json
from sqlitedict import SqliteDict
import os
from pathlib import Path
import re
from pint import UnitRegistry
from pint.errors import UndefinedUnitError

# from nltk.stem.snowball import FrenchStemmer
from french_lefff_lemmatizer.french_lefff_lemmatizer import FrenchLefffLemmatizer
from nltk.tokenize import word_tokenize
from _stopwords import stopwords
from unidecode import unidecode
import requests
from _auth import USR_nextcloud, PWD_nextcloud, URL_nextcloud, ingredient_categories_url
from warnings import warn

import threading
import asyncio
from aiohttp import ClientSession
from aiohttp import BasicAuth

from _homemade_replacements import rep

lemmatizer = FrenchLefffLemmatizer()

ureg = UnitRegistry()
ureg.default_format = ".3f~P"
ureg.define("cas = 15 mL")
ureg.define("cac = 5 mL")
ureg.define("boite = 1 = boit = boites")
Quantity = ureg.Quantity

script_path = Path(os.path.dirname(os.path.abspath(__file__)))
DB_FNME = script_path / "../.database/recipes.sqlite"


def _rm_accents(s):
    return unidecode(s)


def _remove_nested_parens(input_str):
    """Returns a copy of 'input_str' with any parenthesized text removed. Nested parentheses are handled."""
    result = ""
    paren_level = 0
    for ch in input_str:
        if ch == "(":
            paren_level += 1
        elif (ch == ")") and paren_level:
            paren_level -= 1
        elif not paren_level:
            result += ch
    return result


def _reformat(s):
    """
    Reformat s: lower case, replace accents, etc
    Remove text between parentheses
    Stemming
    """
    s = _remove_nested_parens(s)
    s = s.lower().strip()
    # We need to remove the unit, if if is here
    split = re.split("\[(.*?)\]", s)
    s = split[0].strip()
    try:
        unit = split[1]
        unit = f"[{unit}]"
    except IndexError:
        unit = ""
    # Remove stopwords
    for i in rep:
        s = s.replace(*i)
    word_tokens = word_tokenize(s, language="french")
    s = " ".join([w for w in word_tokens if not w in stopwords])
    # Keep only digit and French alphabet chars
    regex = re.compile("[^a-zA-Z0-9\[\]éèêëàâçïîûô]")
    s = regex.sub(" ", s)
    # lemmatize
    s = " ".join([lemmatizer.lemmatize(w) for w in s.split()])
    # Remove accents
    s = _rm_accents(s)
    # add back unit
    unit = _rm_accents(unit)
    s = f"{s} {unit}"
    # Remove multiple spaces
    s = re.sub(" +", " ", s).strip()
    return s


def get_only_ingredient(recipeIngredient):
    return [i.split("[")[0].strip() for i in recipeIngredient]


def contains_ingredient(recipeIngredient, ingredient):
    ingredient = _reformat(ingredient)
    recipeIngredient = get_only_ingredient(recipeIngredient)
    return ingredient in recipeIngredient


list_meat = {
    "lardon",
    "Boulette de viande",
    "steak",
    "cabillaud",
    "dinde",
    "jambon",
    "escalope",
    "filet mignon",
    "thon",
    "viande",
    "poisson",
    "saumon",
    "crevette",
    "poulet",
    "dinde",
    "saucisse",
    "Blodpudding",
    "Chorizo",
    "cordon bleu",
    "Nem",
}
try:
    list_meat = {unidecode(_reformat(i)) for i in list_meat}
except LookupError:
    pass


def contains_meat(ingredients):
    meat = False
    for i in ingredients:
        for m in list_meat:
            if (m in i) or (i in m):
                meat = True
                break
        if meat:
            break
    return meat


def contains_month(recipe_months, month):
    return month in recipe_months


class RunThread(threading.Thread):
    def __init__(self, coro):
        self.coro = coro
        self.result = None
        super().__init__()

    def run(self):
        self.result = asyncio.run(self.coro)


def run_async(coro):
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = None
    if loop and loop.is_running():
        thread = RunThread(coro)
        thread.start()
        thread.join()
        return thread.result
    else:
        return asyncio.run(coro)


## recipes


def get_list_recipes_ids(URL_nextcloud, USR_nextcloud, PWD_nextcloud):
    r = requests.get(
        f"{URL_nextcloud}/apps/cookbook/api/v1/category/Plats",
        auth=(USR_nextcloud, PWD_nextcloud),
    )
    ids = [i["recipe_id"] for i in r.json()]
    # todo add verification to only rebase the new recipes, or the ones that have been modified
    return ids


async def get_recipe_json(session, url):
    response = await session.get(
        url=url, auth=BasicAuth(*(USR_nextcloud, PWD_nextcloud))
    )
    try:
        response_json = await response.json()
    except json.decoder.JSONDecodeError as e:
        response_json = None
    return response_json


async def add_recipe_to_db(recipe, db, products):
    # remove accents, stem, etc, from ingredients
    recipe["recipeIngredientCal"] = recipe.get("recipeIngredient", [])
    recipe["recipeIngredient"] = [
        _reformat(i) for i in recipe.get("recipeIngredient", [])
    ]
    recipe["recipeIngredient"] = [
        i
        for i in recipe["recipeIngredient"]
        if not i in ["sel", "poivre", "huile", "huile d'olive"]
    ]
    # Add months
    months = [
        products.get(i, list(range(1, 13)))
        for i in get_only_ingredient(recipe["recipeIngredient"])
    ]
    out = set(range(1, 13))
    for m in months:
        out = out.intersection(m)
    recipe["months"] = list(sorted(out))
    # Add if contains meat
    recipe["meat"] = contains_meat(get_only_ingredient(recipe["recipeIngredient"]))
    db[recipe["id"]] = recipe


async def get_recipe_and_add_to_db(session, recipe_id, db, products):
    url = f"{URL_nextcloud}/apps/cookbook/api/v1/recipes/{recipe_id}"
    recipe = await get_recipe_json(session, url)
    await add_recipe_to_db(recipe, db, products)


async def fetch_all(session, recipe_ids, db, products):
    tasks = []
    for recipe_id in recipe_ids:
        task = asyncio.create_task(
            get_recipe_and_add_to_db(session, recipe_id, db, products)
        )
        tasks.append(task)
    results = await asyncio.gather(*tasks)
    return results


## categories


async def get_categories(session, url):
    response = await session.get(url=url)
    response = await response.text()
    if "<!DOCTYPE html>" in response:
        return []
    return [
        i.replace(",", ";").split(";")
        for i in response.replace("\r\n", "\n").replace("\r", "\n").split("\n")
    ]


async def add_categories_to_db(categories, db):
    for i in categories:
        if len(i) < 3:
            continue
        db[_reformat(i[0])] = f"{i[1]} -- {i[2]}"


async def get_categories_and_add_to_db(session, db):
    url = ingredient_categories_url
    categories = await get_categories(session, url)
    await add_categories_to_db(categories, db)


async def fetch_categories(session, db):
    tasks = []
    task = asyncio.create_task(get_categories_and_add_to_db(session, db))
    tasks.append(task)
    results = await asyncio.gather(*tasks)
    return results


## Rebase recipes + categories


async def _rebase(db_filename=DB_FNME):
    with open(script_path / "../data/products.json") as f:
        products = json.load(f)
    products = {_reformat(i): products[i] for i in products.keys()}
    db = SqliteDict(db_filename, tablename="recipes")
    recipe_ids = get_list_recipes_ids(URL_nextcloud, USR_nextcloud, PWD_nextcloud)
    async with ClientSession() as session:
        await fetch_all(session, recipe_ids, db, products)
    db.commit()
    db.close()
    # Now get categories
    db = SqliteDict(db_filename, tablename="categories")
    async with ClientSession() as session:
        await fetch_categories(session, db)
    db.commit()
    db.close()


def rebase():
    run_async(_rebase())


def get_db(db_filename=DB_FNME):
    db = SqliteDict(db_filename, tablename="recipes")
    return db


def get_db_categories(db_filename=DB_FNME):
    db = SqliteDict(db_filename, tablename="categories")
    return db


def convert_ingredient(ingredient):
    split = re.split("\[(.*?)\]", ingredient)
    ingredient = split[0].strip().lower()
    try:
        quantity = Quantity(split[1])
    except IndexError:
        quantity = Quantity("1")
    except ValueError:
        # Can happen if problem in units, like having empty []
        quantity = Quantity("1")
    except UndefinedUnitError as e:
        warn(f"Unknown unit in recipe: {e}")
        quantity = Quantity("1")
    return ingredient, quantity
