# This file contains words that we want to be replaced
# e.g. words that are not taken into account by nltk
# e.g. common mistake that we do when we write the ingredients
rep = [
    ("d'", ""),  # d' not detected by nltk
    (" a ", " "),  # a without the accent not detected by nltk
    ("pate", "pâte"),  # Common mistake done
    ("creme", "crème"),
    ("fraiche", "fraîche"),
    ("courge butternut", "butternut"),
    ("courges butternut", "butternut"),
    ("moutarde de dijon", "moutarde"),
    ("moutardes de dijon", "moutarde"),
    ("crème coco", "lait de coco"),
    ("crème de coco", "lait de coco"),
    ("chiches", "chiche"),  # pois chiches not detected
    ("crème liquide", "crème fraîche liquide"),
    ("pâte brisee", "pâte à tarte"),
    ("pâte brisée", "pâte à tarte"),
    ("concassées", "concasse"),
    ("concassees", "concasse"),
    ("concassee", "concasse"),  # attention order matters!
    ("concassée", "concasse"),
    ("séchée", "seche"),
    ("séchées", "seche"),
    ("séchees", "seche"),
    ("séchee", "seche"),
    ("buche", "bûche"),
    ("chevre", "chèvre"),
    ("bûche de chèvre", "chèvre"),
    ("bûchette de chèvre", "chèvre"),
    ("rape", "râpé"),
    ("rapé", "râpé"),
    ("râpe", "râpé"),
    ("fromage râpé", "râpé"),
    ("gruyere", "gruyère"),
    ("gruyère râpé", "râpé"),
    ("gruyère", "râpé"),
    ("diverses", "divers"),
    ("diverse", "divers"),
]
