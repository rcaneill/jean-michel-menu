import caldav
from _auth import (
    USR_nextcloud,
    PWD_nextcloud,
    URL_nextcloud,
    CAL_id_nextcloud,
    N_PEOPLE,
)
from database import convert_ingredient
import icalendar
import datetime
from zoneinfo import ZoneInfo
import yaml
from warnings import warn

client = caldav.DAVClient(
    url=f"{URL_nextcloud}/remote.php/dav",
    username=USR_nextcloud,
    password=PWD_nextcloud,
)
try:
    my_principal = client.principal()
    cal = my_principal.calendar(cal_id=CAL_id_nextcloud)
except Exception as e:
    cal = None
    cal_error = e
    warn(f"Error with calendar. Error may be raised later: {e}")


def create_n_meals(list_ids_start):
    # 1st we want to use a dict so data handling is easier
    meal_plan = {}
    for i in list_ids_start:
        if i[0] in meal_plan:
            meal_plan[i[0]].append([*i, 1])
        else:
            meal_plan[i[0]] = [[*i, 1]]

    for i in meal_plan.keys():
        if len(meal_plan[i]) == 1:
            # no need to do anything as n_meals is set to 1
            continue
        # we compute the time between each meal
        deltas = [
            compute_time_delta(t0[1], t1[1])
            for t0, t1 in zip(meal_plan[i][:-1], meal_plan[i][1:])
        ]
        # we group each meals together if the delta is less than 2 days
        groups = [[meal_plan[i][0][0]]]
        for ii in range(len(deltas)):
            if deltas[ii] <= 2:
                groups[-1].append(meal_plan[i][ii + 1][0])
            else:
                groups.append([meal_plan[i][ii + 1][0]])
        # we can now set n_meals by simply going through groups
        ii = 0
        for g in groups:
            n = len(g)
            for m in g:
                meal_plan[i][ii][2] = n
                ii += 1
    # and last, we unfold the dict
    list_ids_start_n_meals = []
    for i in meal_plan.keys():
        for t in meal_plan[i]:
            list_ids_start_n_meals.append(t)
    return list_ids_start_n_meals


def compute_time_delta(t0, t1):
    try:
        time = abs((t0 - t1).total_seconds() / 86400)  # 3600*24 = 86400
    except TypeError:
        time = abs(
            (t0.replace(tzinfo=None) - t1.replace(tzinfo=None)).total_seconds() / 86400
        )
    return time


def add_recipes(db, ids, dtstarts):
    # 1st we want to make sure that dates are sorted
    list_ids_start = [(i, j) for i, j in zip(ids, dtstarts)]
    list_ids_start.sort(key=lambda x: x[1])
    list_ids_start_n_meals = create_n_meals(list_ids_start)
    for i, (id_recipe, dtstart, n_meals) in enumerate(list_ids_start_n_meals):
        add_recipe_to_cal(
            db,
            id_recipe,
            dtstart,
            dtstart + datetime.timedelta(hours=1),
            n_meals=n_meals,
        )


def create_description_for_recipe(recipe, n_meals):
    description = "---\n\n"
    # Now we scale the ingredients by number of portions and meals
    # 1st we convert to pint quantities
    ingredients = [convert_ingredient(i) for i in recipe["recipeIngredientCal"]]
    # Now we scale
    scale_factor = N_PEOPLE / recipe["recipeYield"] * n_meals
    ingredients = [f"{i} [{j*scale_factor:#~}]" for (i, j) in ingredients]
    description += yaml.dump(
        {"recipeIngredient": ingredients},
        default_flow_style=False,
        allow_unicode=True,
    )
    description += "\n...\n\n"
    description += f"Nombre de portions:\n{N_PEOPLE*n_meals}\n\n"
    description += "Instructions :\n" + "\n".join(recipe["recipeInstructions"])
    return description


def add_recipe_to_cal(db, id_recipe, dtstart, dtend, n_meals=1):
    # get recipe
    recipe = db[id_recipe]
    description = create_description_for_recipe(recipe, n_meals)
    if cal is None:
        raise Exception(cal_error)
    cal.save_event(
        ical="BEGIN:VEVENT\r\nBEGIN:VALARM\r\nACTION:DISPLAY\r\nTRIGGER;RELATED=START:-PT30M\r\nEND:VALARM\r\nEND:VEVENT\r\n",
        summary=recipe["name"],
        description=description,
        location=f"{URL_nextcloud}/apps/cookbook/#/recipe/{id_recipe}",
        dtstart=dtstart,
        dtend=dtend,
    )


def _get_time(i):
    try:
        e = icalendar.Event.from_ical(i.data)
        t = e.subcomponents[0].decoded("dtstart").replace(tzinfo=None)
    except KeyError:
        t = datetime.datetime(2000, 1, 1)
    return t


def get_past_recipes():
    if cal is None:
        raise Exception(cal_error)
    events = cal.events()
    events.sort(key=_get_time, reverse=True)
    ids = []
    t = datetime.datetime.now()
    for i, e in enumerate(events):
        event = icalendar.Event.from_ical(e.data)
        try:
            url = str(event.subcomponents[0].decoded("location"))
            event = icalendar.Event.from_ical(e.data)
            date = event.subcomponents[0].decoded("dtstart")
            out = (date, url.replace("'", "").replace("/", " ").strip().split()[-1])
            try:
                delta_days = abs((t - date).total_seconds() / 86400)  # 3600*24 = 86400
            except TypeError:
                delta_days = abs(
                    (t.replace(tzinfo=None) - date.replace(tzinfo=None)).total_seconds()
                    / 86400
                )
            if delta_days <= 3:
                ids.append(out)
        except KeyError:
            pass
    return ids


def create_next_dates(last_date, n_dates):
    if 10 < last_date.hour < 14:
        meal_type = "lunch"
    else:
        meal_type = "diner"

    if meal_type == "lunch":
        last_date = datetime.datetime(
            last_date.year,
            last_date.month,
            last_date.day,
            12,
            tzinfo=ZoneInfo("Europe/Stockholm"),
        )
    else:
        last_date = datetime.datetime(
            last_date.year,
            last_date.month,
            last_date.day,
            19,
            tzinfo=ZoneInfo("Europe/Stockholm"),
        )

    out = [last_date + datetime.timedelta(int(i / 2) + 1) for i in range(n_dates)]
    # Now every 2nd item we need to change from lunch to diner, or other way around
    if meal_type == "lunch":
        for i in range(0, n_dates, 2):
            out[i] -= datetime.timedelta(days=1)
            out[i] += datetime.timedelta(days=0, hours=7)
    else:
        for i in range(0, n_dates, 2):
            out[i] -= datetime.timedelta(days=0, hours=7)
    return out
